@extends("template.template-user")

@section("content")

@if($errors->any)
    @foreach($errors->all() as $error)
    <div class="alert alert-danger">{{ $error }}</div>
    @endforeach
@endif
<form method="post" action="/pendaki/tambah-anggota/{{ $pemesananId }}">
    @Csrf
    <div class="row g-4 mt-1 settings-section">
        <div class="col-12 col-md-4">
            <h3 class="section-title">Tambah Data Kelompok Pendakiam</h3>
            <div class="section-intro">isikan data lengkap pendaki baru</div>
        </div>
        <div class="col-12 col-md-8">
            <div class="app-card app-card-settings shadow-sm p-4">

                <div class="app-card-body">
                    <input type="hidden" value="{{ $pemesananId }}" name="pemesananId">
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">NIK</label>
                        <input type="text" name="nik" class="form-control" id="setting-input-2" value="{{ session('nik') ?? old('nik') }}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" id="setting-input-1" value="{!! old('nama') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">No Hp</label>
                        <input type="text" name="noHp" class="form-control" id="setting-input-2" value="{!! old('noHp') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Tanggal Lahir</label>
                        <input type="date" name="tanggalLahir" class="form-control" id="setting-input-2" value="{!! old('tanggalLahir') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Jenis Kelamin</label>
                        <select class="form-select" name="jenisKelamin" aria-label="Default select example">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Alamat</label>
                        <input type="text" name="alamat" class="form-control" id="setting-input-2" value="{!! old('alamat') !!}">
                    </div>
                    <button type="submit" class="btn app-btn-primary">Tambah</button>
                </div>
                <!--//app-card-body-->
            </div>
            <!--//app-card-->
        </div>
    </div>

</form>
@endsection
