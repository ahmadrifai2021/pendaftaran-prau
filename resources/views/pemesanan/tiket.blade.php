@extends('template.tempate-home')

@section('content')

<h1 class="app-page-title">Form Pendaftaran Pendakian</h1>
<hr class="mb-4">

@error('tanggalBerangkat')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
@error('tanggalPulang')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
@error('tiketId')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="row g-4 settings-section">
    <div class="col-12 col-md-4">
        <h3 class="section-title">Jadwal Pendakian</h3>
        <div class="section-intro">isikan tanggal berangkat dan pulang untuk dapat memantau kegiatan pendaki</div>
    </div>
    <div class="col-12 col-md-8">
        <div class="app-card app-card-settings shadow-sm p-4">

            <div class="app-card-body">
                <form method="post" action="{{ route('pemesanan.post.tambah') }}">
                    @Csrf
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Tanggal Berangkat</label>
                        <input type="date" name="tanggalBerangkat" class="form-control" id="setting-input-1" value="<?= $_POST['tanggalBerangkat'] ?? '' ?>">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Tanggal Pulang</label>
                        <input type="date" name="tanggalPulang" class="form-control" id="setting-input-2">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Tiket</label>
                        <select class="form-select" name="tiketId" aria-label="Default select example">
                            @foreach($tikets as $tiket)
                            <option value="<?= $tiket->id ?>"><?= $tiket->label ?></option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn app-btn-primary">Submit</button>
                </form>
            </div>
            <!--//app-card-body-->

        </div>
        <!--//app-card-->
    </div>
</div>
<!--//row-->

@endsection
