<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script defer src="{{ asset('/assets/plugins/fontawesome/js/all.min.js') }}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('/assets/css/portal.css') }}">


    <title>Bukti Pendaftaran</title>
    <!-- Fonts -->
    <style>
    </style>
</head>
<body>
    <div class="logo text-center">
        <img src="{{ asset("assets/images/logo.png") }}">
    </div>
    @foreach($pemesanan as $ps)
    <div class="row">
        <div class="tiket text-center">
            <h4>{{ $ps->tiket->label }}</h4>
        </div>
        <div class="qr-code text-center">
            <img src="{{ asset("storage/" . $ps->qr_code) }}" class="img-fluid" >
        </div>
    </div>

    <div class="container mt-2">
        <div class="row">
            <div class="col-8 mx-auto">
                <table class="table">
                    <tr>
                        <td><b>ID Kelompok</b></td>
                        <td><b>:</b></td>
                        <td class="text-uppercase"><b>{{ $ps->id }}</b></td>
                    </tr>
                    <tr>
                        <td><b>Tanggal Berangkat</b></td>
                        <td><b>:</b></td>
                        <td class="text-uppercase"><b>{{ $ps->tanggal_berangkat }}</b></td>
                    </tr>
                    <tr>
                        <td><b>Tanggal Pulang</b></td>
                        <td><b>:</b></td>
                        <td class="text-uppercase"><b>{{ $ps->tanggal_pulang }}</b></td>
                    </tr>
                    <tr>
                        <td><b>Anggota</b></td>
                        <td><b>:</b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Nomer Hp</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ps->pendaki as $pendaki)
                    <tr>
                        <th scope="row"></th>
                        <th>{{ $pendaki->nama }}</th>
                        <td>{{ $pendaki->alamat }}</td>
                        <td>{{ $pendaki->nomer_hp }}</td>
                        <td class="text-uppercase">{{ $pendaki->pivot->status_pendaki }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Total Pembayaran Tiket : Rp. {{ count($ps->pendaki) * $ps->tiket->harga }}</b>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="container mt-2 px-5">
        <div class="row">
            <div class="col-12">
                <h3>Catatan</h3>
                <ul>
                    <li>Bukti pendaftaran harap dibawa</li>
                    <li>Membawa fotocopy kartu identitas ketua kelompok</li>
                    <li>Harap membawa uang sebesar <b>Rp . {{ count($ps->pendaki) * $ps->tiket->harga }}</b> untuk pembayaran tiket</li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach

    <!-- Javascript -->
    <script src="{{asset('/assets/plugins/popper.min.js')}}"></script>
    <script src="{{asset('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Page Specific JS -->
    <script src="{{asset('/assets/js/app.js')}}"></script>
</body>
</html>
