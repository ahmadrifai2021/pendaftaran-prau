<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ $title ?? "Pendaftaran Pendakian PRAU" }}</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- FontAwesome JS-->
    <script defer src="{{ asset('/assets/plugins/fontawesome/js/all.min.js') }}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('/assets/css/portal.css') }}">

    <style>
        nav{
            margin-top: -60px;
        }
    </style>

</head>

<body class="app">
<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm nav-custom">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="{{ asset("/assets/images/logo.png") }}" class="img-fluid" width="50px">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{ url("/") }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url("/sop") }}">SOP</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./seminar.html">Tutorial</a>
                </li>
            </ul>
            <form class="d-flex">
                <a href="{{ url('/user/login') }}" class="btn app-btn-secondary rounded-0" type="submit">Login</a>
                <a href="{{ url('/user/daftar') }}" class="btn app-btn-primary mx-1 rounded-0 bg-hijau" type="submit">Daftar</a>
            </form>
        </div>
    </div>
</nav>
<!-- //navbar -->
<div class="container-fluid bg-white">
    <div class="row text-center">
        <div class="col-md-12">
            <img src="{{ asset('/assets/images/logo.png') }}" alt="">
            <h3>PENDAFTARAN ONLINE PENDAKIAN GUNUNG PRAU 2565 MDPL</h3>
        </div>
    </div>
</div>
<div class="app">

    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">

            @yield('content')

        </div>
        <!--//container-fluid-->
    </div>
    <!--//app-content-->

    <footer class="app-footer">
        <div class="container text-center py-3">
            <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
            <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i> by <a class="app-link" href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>

        </div>
    </footer>
    <!--//app-footer-->

</div>
<!--//app-wrapper-->


<!-- Javascript -->
<script src="{{asset('/assets/plugins/popper.min.js')}}"></script>
<script src="{{asset('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Page Specific JS -->
<script src="{{asset('/assets/js/app.js')}}"></script>

</div><!--//row-->


</body>
</html>
