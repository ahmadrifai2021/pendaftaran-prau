@extends("template.tempate-home")


@section("content")
    <div class="app-card alert alert-dismissible shadow-sm mb-4 border-left-decoration" role="alert">
        <div class="inner">
            <div class="app-card-body p-3 p-lg-4">
                <h3 class="mb-3">Selamat Datang</h3>
                <div class="row gx-5 gy-3">
                    <div class="col-12 col-lg-9">

                        <div>
                            Untuk dapat melakukan pendaftaran silahkan baca ketentuan standar operasional dan menyetujui peraturan yang telah tertera
                            <br>
                            <a href="{{ route("sop") }}" class="btn app-btn-primary">Baca SOP</a>
                        </div>
                    </div>
                    <!--//col-->
                </div>
                <!--//row-->
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <!--//app-card-body-->

        </div>
        <!--//inner-->
    </div>

@endsection
