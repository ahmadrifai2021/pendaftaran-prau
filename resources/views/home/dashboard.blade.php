@extends("template.template-user")

@section("content")


@if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
@endif
{{--{{ dd($pemesanan) }}--}}

{{-- <img src="{{ asset("storage/".$pesanan->qr_code) }}">--}}
<div class="app-card alert alert-dismissible shadow-sm mb-4 border-left-decoration" role="alert">
    <div class="inner">
        <div class="app-card-body p-3 p-lg-4">
            <h3 class="mb-3">Selamat Datang, {{ $pendaki->nama }}</h3>
            <div class="row gx-5 gy-3">
                <div class="col-12 col-lg-9">

                    <div>Silahkan daftar pemesanan atau tambah anggota pendaki jika sudah membuat pemesanan tiket pendakian </div>
                </div><!--//col-->
            </div><!--//row-->
        </div><!--//app-card-body-->

    </div><!--//inner-->
</div><!--//app-card-->

<h1 class="app-page-title">Pesanan Tiket Pendakian</h1>

@foreach($pemesanan as $ps)
<div class="row g-4 mb-4">
    <div class="col-12 col-lg-8">
        <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-receipt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"/>
                                <path fill-rule="evenodd" d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </div><!--//icon-holder-->

                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">{{ $ps->tiket->label }}</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <div class="app-card-body px-4">
                <div class="row mb-4">
                    <div class="col-12">
                        <div class="item py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong> Tanggal Berangkat </strong></div>
                                    <div class="item-data">{{$ps->tanggal_berangkat}}</div>
                                </div><!--//col-->
                                <div class="col text-end">
                                    <div class="item-label"><strong> Tanggal_Pulang </strong></div>
                                    <div class="item-data">{{$ps->tanggal_pulang}}</div>
                                </div><!--//col-->
                            </div><!--//row-->
                        </div><!--//item-->
                    </div>
                </div>
                   <div class="row g-4">
                       <div class="col-12 col-lg-4">
                           <img src="{{ asset("storage/" . $ps->qr_code) }}" width="250px" class="img-fluid">
                       </div>
                       <div class="col-12 col-lg-7">
                           <h5>Daftar Anggota Kelompok</h5>
                           @foreach($ps->pendaki as $pd)
                               <div class="item border-bottom py-3">
                                   <div class="row justify-content-between align-items-center">
                                       <div class="col-auto">
                                           <div class="item-label"><strong>{{$pd->nama}} </strong></div>
                                           {{ $pd->pivot->status_pendaki }}

                                       </div><!--//col-->
                                       <div class="col text-end">
                                           @if( $pd->pivot->status_pendaki != "ketua")
                                               <a class="btn-sm app-btn-danger text-danger" href="/pemesanan/hapus/{{ $pd->pivot->id }}">Hapus</a>
                                           @endif
                                           <a class="btn-sm app-btn-secondary" href="#">Detail</a>
                                       </div><!--//col-->
                                   </div><!--//row-->
                               </div><!--//item-->
                           @endforeach
                       </div>
                   </div>
                </div><!--//app-card-body-->
            <div class="app-card-footer p-4 mt-auto">
                <a class="btn app-btn-primary" href="/pemesanan/cetak-pdf/{{ $ps->id }}">Cetak Bukti Pendaftaran</a>
                <button class="btn app-btn-secondary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Tambah Anggota Kelompok
                </button>
                <!-- Modal -->
                <form method="post" action="{{ route("pendaki.cekNik") }}">
                    @Csrf
                    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="pemesananId" value="{{ $ps->id }}">
                                    <div class="mb-3">
                                        <label for="setting-input-1" class="form-label">NIK</label>
                                        <input type="text" name="nik" class="form-control" id="setting-input-1" placeholder="Masukan NIK untuk menambah anggota kelompok" value="<?= $_POST['nik'] ?? '' ?>">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn app-btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn app-btn-primary">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div><!--//app-car
            d-footer-->
        </div><!--//app-card-->
        @if(isset($pemesananNotFound))
            <div class="app-card alert alert-dismissible shadow-sm mb-4 border-left-decoration" role="alert">
                <div class="inner">
                    <div class="app-card-body p-3 p-lg-4">
                        <div class="row gx-5 gy-3">
                            <div class="col-12 col-lg-9">
                                <div>{{ $pemesananNotFound }} </div>
                            </div><!--//col-->
                        </div><!--//row-->
                    </div><!--//app-card-body-->

                </div><!--//inner-->
            </div><!--//app-card-->
        @endif
    </div><!--//col-->
</div><!--//row-->
@endforeach
@endsection
