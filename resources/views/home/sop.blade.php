@extends("template.tempate-home")

@section("content")
    @error('setuju')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="container-fluid bg-info p-4">
        <div class="row text-center">
            <div class="col">
                <h3>SOP</h3>
            </div>
        </div>
    </div>


    <div class="container pt-3">
        <div class="row">
            <div class="col">


                <h4>Standar Operasional Prosedur</h4>
                <p>
                    Keputusan tersebut disampaikan berdasarkan Surat Forum Koordinasi Gunung Prau Indonesia (FKPI) Nomor 2020/FKPI-08/01 pada tanggal 7 Agustus 2020, dalam hal pemberitahuan jalur pendakian dalam masa uji coba tahap kedua. Namun, para pengunjung yang ingin menyambangi Gunung Prau di Jawa Tengah wajib mematuhi berbagai peraturan yang berlaku. Berikut Tagar ulas syarat mendaki pada masa uji coba tahap kedua Gunung Prau.

                <ul>
                    <li>Membawa Surat Keterangan Sehat Para pendaki berasal dari daerah mana saja diharuskan untuk membawa surat keterangan sehat yang nantinya ditunjukkan saat registrasi pendakian. Jika belum memilikinya, para pendaki bisa mendapatkannya di puskesmas basecamp Gunung Prau seperti Patak Banteng.
                    </li>
                    <li>
                        Membawa Kartu Identitas Para pendaki yang ingin menyambangi Gunung Prau di Jawa Tengah wajib membawa kartu identitas dirinya. Selain kartu identitas, seperti KTP atau yang lainnya, para pendaki juga harus membawa fotokopinya.
                    </li>
                    <li>
                        Membawa Hand Sanitizer Selain mencuci tangan dengan sabun dan air mengalir, para pendaki diharuskan membawa hand sanitizer. Sebab, ini menjadi salah satu cara pencegahan penyebaran virus Corona atau Covid-19. Sehingga, pendakian bisa tetap aman dan nyaman.
                    </li>
                    <li>
                        Membawa Masker Cadangan Para pendaki yang ingin berkunjung ke Gunung Prau di Jawa Tengah juga diharuskan membawa masker dan cadangannya. Ini bertujuan agar masker bisa diganti setelah dipakai setidaknya selama empat jam.
                    </li>
                    <li>
                        Membawa Perlengkapan Pendakian Sesuai SOP Para pendaki juga diwajibkan membawa perlengkapan sesuai SOP yang diterapkan, seperti tenda, jaket, baju ganti, sarung tangan, peralatan dan bahan memasak, sleeping bag, matras, dan obat. Ini guna menghindari terjadinya hal yang tidak diinginkan seperti hipotermia mengingat cuaca di Gunung Prau sedang dingin.
                    </li>
                    <li>
                        Kuota Pendakian 50 Persen dari Kapasitas Normal Selama masa uji coba tahap kedua, kuota pendakian Gunung Prau masih dibatasi. Para pengunjung akan dibatasi sebesar 50 persen dari kuota normal biasanya.
                    </li>
                    <li>
                        Tenda Diisi 50 Persen dari Kapasitas Normal Tenda yang dibawa oleh para pendaki juga dibatasi dengan hanya boleh diisi sebanyak 50 persen dari kapasitas normal. Misalnya, tenda tersebut berkapasitas untuk empat orang, di era new normal sekarang ini tenda hanya boleh diisi paling banyak dua orang.
                    </li>
                    <li>
                        Peraturan Lama Masih Berlaku Di era new normal, peraturan pendakian Gunung Prau yang ada dari sebelum pandemi juga masih berlaku. Sehingga, para pendaki tetap diwajibkan menerapkan aturan tersebut.
                    </li>
                </ul>
                </p>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Peraturan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Dilarang Menebang Pohon</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Dilarang Membuta Api Unggun</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Dilarang Melakukan Vandalisme</td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>Dilarang Memberi Makan Satwa Habitat</td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>Dilarang Membuang Sampah Sembarangan</td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td>Sampah Pribadi Wajib Dibawa Turun </td>
                    </tr>
                    </tbody>
                </table>
                <form action="{{ route("post.sop") }}" method="post">
                    @Csrf
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="setuju" value="true" id="flexCheckDefault" >
                        <label class="form-check-label" for="flexCheckDefault">
                            Setuju
                        </label>
                    </div>

                    <button type="submit" class="btn app-btn-primary btn-sm mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
