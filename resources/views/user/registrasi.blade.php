@extends("template.tempate-home")

@section("content")
<h1 class="app-page-title">Form Pendaftaran Pendakian</h1>
<hr class="mb-4">

@if(isset($errors))
@foreach($errors->all() as $message)
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@endforeach
@endif

<form method="post" action="/user/registrasi/{{ $pemesananId }}">
    @Csrf
    <div class="row g-4 mt-1 settings-section">
        <div class="col-12 col-md-4">
            <h3 class="section-title">Data ketua Kelompok</h3>
            <div class="section-intro">isikan data lengkap anda untuk dapat melakukan pendaftan ketua kelompok</div>
        </div>
        <div class="col-12 col-md-8">
            <div class="app-card app-card-settings shadow-sm p-4">

                <div class="app-card-body">
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">NIK</label>
                        <input type="text" name="nik" class="form-control" id="setting-input-2" value="{!! old('nik') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" id="setting-input-1" value="{!! old('nama') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">No Hp</label>
                        <input type="text" name="noHp" class="form-control" id="setting-input-2" value="{!! old('noHp') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Tanggal Lahir</label>
                        <input type="date" name="tanggalLahir" class="form-control" id="setting-input-2" value="{!! old('tanggalLahir') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Jenis Kelamin</label>
                        <select class="form-select" name="jenisKelamin" aria-label="Default select example">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Alamat</label>
                        <input type="text" name="alamat" class="form-control" id="setting-input-2" value="{!! old('alamat') !!}">
                    </div>
                </div>
                <!--//app-card-body-->

            </div>
            <!--//app-card-->
        </div>
    </div>
    <div class="row g-4 mt-1 settings-section">
        <div class="col-12 col-md-4">
            <h3 class="section-title">Daftara User</h3>
            <div class="section-intro">Daftar user untuk dapar melanjutkan pendaftaran ketua</div>
        </div>
        <div class="col-12 col-md-8">
            <div class="app-card app-card-settings shadow-sm p-4">

                <div class="app-card-body">
                    <div class="mb-3">
                        <label for="setting-input-2" class="form-label">Username</label>
                        <input type="text" name="username" class="form-control" id="setting-input-2" value="{!! old('username') !!}">
                    </div>
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="setting-input-1" value="{!! old('password') !!}">
                    </div>

                    <button type="submit" class="btn app-btn-primary">Daftar</button>
                </div>
                <!--//app-card-body-->

            </div>
            <!--//app-card-->
        </div>
    </div>
</form>
@endsection
