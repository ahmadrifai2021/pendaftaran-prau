@extends("template.template-auth")

@section("content")


<div class="app-auth-branding mb-4"><a class="app-logo" href="index.html"><img class="logo-icon me-2" src="{{asset('assets/images/logo.png')}}" alt="logo"></a></div>
<h2 class="auth-heading text-center mb-5">Log in User</h2>

@if(isset($errors))
    @foreach($errors->all() as $message)
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @endforeach
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

<div class="auth-form-container text-start">
    <form class="auth-form login-form" method="post" action="{{ route("user.post.login")  }}">
        {{ csrf_field() }}
        <div class="email mb-3">
            <label class="sr-only" for="signin-email">Username</label>
            <input id="signin-email" name="username" type="text" class="form-control signin-email" placeholder="Username">
        </div><!--//form-group-->
        <div class="password mb-3">
            <label class="sr-only" for="signin-password">Password</label>
            <input id="signin-password" name="password" type="password" class="form-control signin-password" placeholder="Password">
        </div><!--//form-group-->
        <div class="text-center">
            <button type="submit" class="btn app-btn-primary w-100 theme-btn mx-auto">Log In</button>
        </div>
    </form>

    <div class="auth-option text-center pt-5">Belum Punya Akun? Daftar <a class="text-link" href="{{ route('user.register-account') }}" >di sini</a>.</div>
</div><!--//auth-form-container-->



@endsection
