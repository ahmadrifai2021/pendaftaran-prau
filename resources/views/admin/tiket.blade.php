@extends("template.template-admin")

@section("content")
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<button class="btn app-btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
Tambah Tiket
</button>
<div class="tab-content" id="orders-table-tab-content">
<div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
    <div class="app-card app-card-orders-table shadow-sm mb-5">
        <div class="app-card-body">
            <div class="table-responsive">
                <table class="table app-table-hover mb-0 text-left">
                    <thead>
                    <tr>
                        <th class="cell">Tiket</th>
                        <th class="cell">Harga</th>
                        <th class="cell">action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tiket as $tk)
                        <tr>
                            <td class="cell">{{ $tk->label }}</td>
                            <td class="cell">Rp. {{ $tk->harga }}</td>
                            <td class="cell">
                                <a href="{{ route("tiket.delete", ["id" => $tk->id]) }}" class="btn btn-sm btn-danger">Hapus</a>
                                <a href="#" class="btn btn-sm btn-info">Edit</a>
                            </td>
                      </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!--//table-responsive-->

        </div><!--//app-card-body-->
    </div><!--//app-card-->


</div><!--//tab-pane-->

<form method="post" action="{{ route("tiket.add") }}">
    @Csrf
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Label</label>
                        <input type="text" name="label" class="form-control" id="setting-input-1" placeholder="ex: Tiket Pendakian" value="{{ old("label") }}">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Harga</label>
                        <input type="number" name="harga" class="form-control" id="setting-input-1" placeholder="Harga" value="{{ old("harga") }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn app-btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn app-btn-primary">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection
