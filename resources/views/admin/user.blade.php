@extends("template.template-admin")

@section("content")
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="app-card alert alert-dismissible shadow-sm mb-4 border-left-decoration" role="alert">
    <div class="inner">
        <div class="app-card-body p-3 p-lg-4">
            <div class="row gx-5 gy-3">
                <div class="col-12 col-lg-9">
                    <button class="btn app-btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                        Tambah User Admin
                    </button>
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Admin</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">User</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#ID</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Role</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($admin as $ad)
                                    <tr>
                                        <td class="cell">#{{ $ad->id }}</td>
                                        <td class="cell">{{ $ad->username }}</td>
                                        <td class="cell">{{ $ad->role }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#ID</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Role</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user as $usr)
                                    <tr>
                                        <td class="cell">#{{ $usr->id }}</td>
                                        <td class="cell">{{ $usr->username }}</td>
                                        <td class="cell">{{ $usr->role }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{ $user->links() }}
                        </div>
                    </div>
                </div><!--//col-->
            </div><!--//row-->
        </div><!--//app-card-body-->

    </div><!--//inner-->
</div><!--//app-card-->

<!-- Modal -->
<form method="post" action="{{ route("user.add.admin") }}">
    @Csrf
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" id="setting-input-1" placeholder="Nama" value="{{ old("nama") }}">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Email</label>
                        <input type="text" name="email" class="form-control" id="setting-input-1" placeholder="Email" value="{{ old("email") }}">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Nomer HP</label>
                        <input type="text" name="nomer_hp" class="form-control" id="setting-input-1" placeholder="Nomer Hp" value="{{ old("nomer_hp") }}">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Username</label>
                        <input type="text" name="username" class="form-control" id="setting-input-1" placeholder="Username" value="{{ old("username") }}">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="setting-input-1" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="setting-input-1" placeholder="Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn app-btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn app-btn-primary">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</form>




@endsection

