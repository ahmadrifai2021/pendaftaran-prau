@extends("template.template-admin")

@section("content")
    <div class="tab-content" id="orders-table-tab-content">
        <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
            <div class="app-card app-card-orders-table shadow-sm mb-5">
                <div class="app-card-body">
                    <div class="table-responsive">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                            <tr>
                                <th class="cell">ID</th>
                                <th class="cell">NIK</th>
                                <th class="cell">Nama</th>
                                <th class="cell">Alamat</th>
                                <th class="cell">No Hp</th>
                                <th class="cell">Jenis Kelamin</th>
                                <th class="cell">Tanggal Lahir</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pendaki as $pd)
                                <tr>
                                    <td class="cell">#{{ $pd->id }}</td>
                                    <td class="cell">{{ $pd->nik }}</td>
                                    <td class="cell">{{ $pd->nama }}</td>
                                    <td class="cell">{{ $pd->alamat }}</td>
                                    <td class="cell">{{ $pd->nomer_hp }}</td>
                                    <td class="cell">{{ $pd->jenis_kelamin }}</td>
                                    <td class="cell">{{ $pd->tanggal_lahir }}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!--//table-responsive-->
                </div><!--//app-card-body-->

            </div><!--//app-card-->

            {{ $pendaki->links() }}


        </div><!--//tab-pane-->

@endsection
