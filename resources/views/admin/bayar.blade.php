@extends("template.template-admin")

@section("content")


{{--    @foreach($pemesanan as $ps)--}}
<div class="row g-4 mb-4">
    <div class="col-12 col-lg-8">
        <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-receipt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"/>
                                <path fill-rule="evenodd" d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </div><!--//icon-holder-->

                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">{{ $pemesanan->tiket->label }}</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <div class="app-card-body px-4">
                <div class="row">
                    <div class="qr-code text-center">
                        <img src="{{ asset("storage/" . $pemesanan->qr_code) }}" class="img-fluid" >
                    </div>
                </div>

                <div class="container mt-2">
                    <div class="row">
                        <div class="col-8 mx-auto">
                            <table class="table">
                                <tr>
                                    <td><b>ID Kelompok</b></td>
                                    <td><b>:</b></td>
                                    <td class="text-uppercase"><b>{{ $pemesanan->id }}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Tanggal Berangkat</b></td>
                                    <td><b>:</b></td>
                                    <td class="text-uppercase"><b>{{ $pemesanan->tanggal_berangkat }}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Tanggal Pulang</b></td>
                                    <td><b>:</b></td>
                                    <td class="text-uppercase"><b>{{ $pemesanan->tanggal_pulang }}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Anggota</b></td>
                                    <td><b>:</b></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-8 mx-auto">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Nomer Hp</th>
                                    <th scope="col">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pemesanan->pendaki as $pendaki)
                                    <tr>
                                        <th scope="row"></th>
                                        <th>{{ $pendaki->nama }}</th>
                                        <td>{{ $pendaki->alamat }}</td>
                                        <td>{{ $pendaki->nomer_hp }}</td>
                                        <td class="text-uppercase">{{ $pendaki->pivot->status_pendaki }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <b>Total Pembayaran Tiket : Rp. {{ count($pemesanan->pendaki) * $pemesanan->tiket->harga }}</b>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <div class="app-card-footer p-4 mt-auto">
                <form method="post" action="{{ route("pemesanan.post.bayar") }}">
                    @Csrf
                    <input type="hidden" name="id_pemesanan" value="{{ $pemesanan->id }}">
                    @if($pemesanan->status_pembayaran == "BELUM_BAYAR")
                        <button class="btn app-btn-primary" type="submit">Terbayar</button>
                    @else
                        <button class="btn app-btn-secondary disabled" type="button">Sudah dibayar</button
                    @endif

                </form>
            </div><!--//app-car
    d-footer-->
        </div><!--//app-card-->
    </div><!--//col-->
</div><!--//row-->
{{--    @endforeach--}}
@endsection

{{--@endsection--}}
