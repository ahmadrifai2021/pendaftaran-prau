@extends("template.template-admin")

@section("content")
    <div class="tab-content" id="orders-table-tab-content">
        <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
            <div class="app-card app-card-orders-table shadow-sm mb-5">
                <div class="app-card-body">
                    <div class="table-responsive">
                        <table class="table app-table-hover mb-0 text-left">
                            <thead>
                            <tr>
                                <th class="cell">ID</th>
                                <th class="cell">Status</th>
                                <th class="cell">Tiket</th>
                                <th class="cell">Tanggal Pemesanan</th>
                                <th class="cell">Tanggal Berangkat</th>
                                <th class="cell">Tanggal Pulang</th>
                                <th class="cell"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pemesanan as $ps)
                            <tr>
                                <td class="cell">#{{ $ps->id }}</td>
                                @if($ps->status_pembayaran == "TERBAYAR")
                                    <td class="cell"><span class="badge bg-success">Terbayar</span></td>
                                @else
                                    <td class="cell"><span class="badge bg-danger">Belum Terbayar</span></td>
                                @endif
                                <td class="cell">{{ $ps->tiket->label }}</td>
                                <td class="cell"><span>{{ $ps->created_at }}</span></td>
                                <td class="cell"><span>{{ $ps->tanggal_berangkat }}</span></td>
                                <td class="cell"><span>{{ $ps->tanggal_pulang}}</span></td>
                                @if($ps->status_pembayaran == "TERBAYAR")
                                    <td class="cell"><a class="btn-sm app-btn-secondary" href="{{ route("pemesanan.bayar", ["id" => $ps->id]) }}">View</a></td>
                                @else
                                    <td class="cell"><a class="btn-sm app-btn-primary" href="{{ route("pemesanan.bayar", ["id" => $ps->id]) }}">Bayar</a></td>
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!--//table-responsive-->

                </div><!--//app-card-body-->
            </div><!--//app-card-->
            {{ $pemesanan->links() }}


        </div><!--//tab-pane-->

@endsection
