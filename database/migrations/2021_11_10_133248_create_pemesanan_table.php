<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->string("id")->primary();
            $table->date("tanggal_berangkat");
            $table->date("tanggal_pulang");
            $table->string("tiket_id");
            $table->foreign("tiket_id")
                ->references("id")->on("tiket");
            $table->text("qr_code");
            $table->enum("status_pembayaran", ["TERBAYAR", "BELUM_BAYAR"])
                ->default("BELUM_BAYAR");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}
