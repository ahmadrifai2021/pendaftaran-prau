<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendakiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaki', function (Blueprint $table) {
            $table->string("id")->primary();
            $table->string("nik", 20);
            $table->string("nama");
            $table->string("alamat");
            $table->string("nomer_hp");
            $table->enum("jenis_kelamin", ["L", "P"]);
            $table->date("tanggal_lahir");
            $table->string("user_id")->nullable();
            $table->foreign("user_id")
                ->references("id")->on("user");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaki');
    }
}
