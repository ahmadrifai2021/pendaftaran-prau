<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananPendakiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan_pendaki', function (Blueprint $table) {
            $table->id();
            $table->string("pendaki_id");
            $table->foreign("pendaki_id")
                ->references("id")->on("pendaki");
            $table->string("pemesanan_id");
            $table->foreign("pemesanan_id")
                ->references("id")->on("pemesanan");
            $table->string("status_pendaki");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan_pendaki');
    }
}
