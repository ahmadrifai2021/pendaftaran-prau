<?php

use App\Model\Pendaki;
use App\Model\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();
        Pendaki::truncate();

        $user = new User();
        $user->id = uniqid();
        $user->username = "test-user";
        $user->password = \Illuminate\Support\Facades\Hash::make("user");
        $user->role = "user";
        $user->save();

        $user2 = new User();
        $user2->id = uniqid();
        $user2->username = "admin";
        $user2->password = \Illuminate\Support\Facades\Hash::make("admin");
        $user2->role = "admin";
        $user2->save();

        $pendaki = new Pendaki();
        $pendaki->id = uniqid("PENDAKI-");
        $pendaki->nik = "202108117090001";
        $pendaki->nama = "test-user";
        $pendaki->alamat = "test";
        $pendaki->nomer_hp = "085xxx";
        $pendaki->jenis_kelamin = "L";
        $pendaki->jenis_kelamin = "L";
        $pendaki->tanggal_lahir = "2020-10-10";
        $pendaki->user_id = $user->id;
        $pendaki->save();

        $admin = new \App\Model\Admin();
        $admin->id = uniqid("ADMIN-");
        $admin->nama = "lutfi";
        $admin->email = "lutfi@gmail.com";
        $admin->nomer_hp = "08543234344231";
        $admin->user_id = $user2->id;
        $admin->save();
     }
}
