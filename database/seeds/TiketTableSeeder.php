<?php

use Illuminate\Database\Seeder;

class TiketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Model\Tiket::truncate();

        $tiket = new \App\Model\Tiket();
        $tiket->id = uniqid();
        $tiket->label = "Pendakian Gunung Prau";
        $tiket->harga = 10000;
        $tiket->save();

    }
}
