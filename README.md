
# Pemesanan Tiket Pendakian

## Requirement
- php >= 7.4
- composer
## instalasi
- buat database "pemesanan-tiket-pendakian"

- Install dependensi composer
```shell
composer install
```

- tambah key laravel app
```shell
php artisan key:generate
```

- migrasi table ke database dan membuat seeding untuk data pertama
```shell
php artisan migrate # migrasi
php artisan db:seed # seeding
```
- membuat storage link
```shell
php artisan storage:link
```
