<?php

use Illuminate\Support\Facades\Route;


// Home
Route::get("/", "HomeController@index")->name("index");
Route::get("/sop", "HomeController@sop")->name("sop");
Route::post("/sop", "HomeController@postSop")->name("post.sop");

// Home
Route::prefix("/home")->name("home.")->group(function (){
    Route::get("/dashboard","HomeController@dashboard")->name("dashboard")->middleware("mustLogin");
});

// admin
Route::prefix("/admin")->name("admin.")->group(function (){
    Route::get("/dashboard", "AdminController@dashboard")->name("dashboard");
    Route::get("/pemesanan", "AdminController@pemesanan")->name("pemesanan");
    Route::get("/tiket", "AdminController@tiket")->name("tiket");
    Route::get("/pendaki", "AdminController@pendaki")->name("pendaki");
    Route::get("/user", "AdminController@user")->name("user");
});

// Pemesanan Router
Route::prefix("/pemesanan")->name("pemesanan.")->group(function (){
    Route::get("/tambah", "PemesananController@tambah")->name("tambah");
    Route::post("/tambah","PemesananController@postTambah")->name("post.tambah");
    Route::get("/cetak-pdf/{pemesananId}", "PemesananController@cetakPdf")->name("cetakPdf");
    Route::get("/hapus/{pemesanan_pendaki}", "PemesananController@hapus")->name("hapus");
    Route::get("/riwayat", "PemesananController@riwayat")->name("riwayat");
    Route::get("/bayar/{id}", "PemesananController@bayar")->name("bayar")->middleware("mustLogin");
    Route::post("/bayar", "PemesananController@postBayar")->name("post.bayar")->middleware("mustLogin");
});

// User Router
Route::prefix("/user")->name("user.")->group(function (){
    Route::get("/login", "UserController@login")->name("login")->middleware("mustNotLogin");
    Route::post("/login", "UserController@postLogin")->name("post.login");
    Route::get("/registrasi/{pemesananId}", "UserController@formRegistrasi")->name("registrai");
    Route::post("/registrasi/{pemesananId}", "UserController@postRegistrasi")->name("post.registrasi");
    Route::get("/logout", "UserController@logout")->name("logout");
    Route::post("/add-admin", "UserController@addAdmin")->name("add.admin");
    Route::get("/register-account", "UserController@registerAccount")->name("register-account");
    Route::post("/register-account", "UserController@postRegisterAccount")->name("post.register-account");
});

// Pendaki Route
Route::prefix("/pendaki")->name("pendaki.")->group(function (){
    Route::post("/cek-nik", "PendakiController@cekNik")->name("cekNik");
    Route::get("/tambah-anggota/{pemesananId}", "PendakiController@tambahAnggota")->name("tambah.anggota");
    Route::post("/tambah-anggota/{pemesananId}", "PendakiController@postTambahAnggota")->name("post.tambah.anggota");
});

// Tiket
Route::prefix("/tiket")->name("tiket.")->group(function (){
    Route::post("/add", "TiketController@add")->name("add");
    Route::get("/delete/{id}", "TiketController@delete")->name("delete");
});
