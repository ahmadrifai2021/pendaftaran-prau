<?php

namespace App\Service;

use App\Http\Requests\PendakiTambahAnggotaByNikRequest;
use App\Http\Requests\PendakiTambahAnggotaRequest;
use App\Model\Response\PendakiTambahAnggotaResponse;

interface PendakiService
{
    public function tambahAnggota(PendakiTambahAnggotaRequest $requst) : PendakiTambahAnggotaResponse;
    public function tambahAnggotaByNIK(PendakiTambahAnggotaByNikRequest $request) : PendakiTambahAnggotaResponse;
}
