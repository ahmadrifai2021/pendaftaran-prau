<?php

namespace App\Service;


use App\Http\Requests\BayarPemesananRequest;
use App\Http\Requests\PemesananTiketRequest;
use App\Model\Response\BayarPemesananResponse;
use App\Model\Response\PemesananTiketResponse;

interface PemesananService
{
    public function tambah(PemesananTiketRequest $request) : PemesananTiketResponse;

    public function bayar(BayarPemesananRequest $request) : BayarPemesananResponse;
}
