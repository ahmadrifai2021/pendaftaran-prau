<?php

namespace App\Service;

use App\Model\Admin;
use App\Model\Pendaki;
use App\Model\Session;

interface SessionService
{
    public function create(string $userId): Session;

    public function current(): ?Pendaki;

    public function destroy(): void;

    public function currentAdmin() : ?Admin;
}
