<?php

namespace App\Service\Impl;

use App\Http\Requests\AddTiketRequest;
use App\Model\Tiket;
use App\Service\TiketService;

class TiketServiceImpl implements TiketService
{

    public function add(AddTiketRequest $request): Tiket
    {
        $tiket = new Tiket();
        $tiket->id = uniqid();
        $tiket->label = $request->label;
        $tiket->harga = $request->harga;
        $tiket->save();

        return $tiket;
    }

    public function delete(string $id): Tiket
    {
        $tiket = Tiket::find($id);
        $tiket->delete();

        return $tiket;
    }
}
