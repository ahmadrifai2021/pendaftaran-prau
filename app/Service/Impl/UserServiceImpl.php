<?php

namespace App\Service\Impl;

use App\Exceptions\PemesananNotFoundException;
use App\Exceptions\UserAlreadyExistException;
use App\Exceptions\UserException;
use App\Http\Requests\AccountRegisterRequest;
use App\Http\Requests\AddUserAdminRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrasiPendakiRequest;
use App\Model\Admin;
use App\Model\Pemesanan;
use App\Model\Pendaki;
use App\Model\Response\UserFormRegistrasiPendakiRequest;
use App\Model\Response\UserFormRegistrasiPendakiResponse;
use App\Model\Response\UserLoginResponse;
use App\Model\Response\UserRegistrasiPendakiResponse;
use App\Model\User;
use App\Service\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PHPUnit\Exception;


class UserServiceImpl implements UserService
{

    public function formRegistrasiPendaki(string $pemesananId): UserFormRegistrasiPendakiResponse
    {
        $pemesanan = Pemesanan::find($pemesananId);

        if ($pemesanan == null){
            throw new PemesananNotFoundException("melakukan pemesanan dahulu");
        }

        $response = new UserFormRegistrasiPendakiResponse();
        $response->pemesanan = $pemesanan;
        return $response;
    }

    public function registrasiPendaki(UserRegistrasiPendakiRequest $request): UserRegistrasiPendakiResponse
    {
        try {
            DB::beginTransaction();

            // tambah pendaki dan user
            $user = new User();
            $user->id = uniqid("USER");
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->role = "user";
            $user->save();

            $pendaki = new Pendaki();
            $pendaki->id = uniqid("PD");
            $pendaki->nik = $request->nik;
            $pendaki->nama = $request->nama;
            $pendaki->alamat = $request->alamat;
            $pendaki->jenis_kelamin = $request->jenisKelamin;
            $pendaki->nomer_hp = $request->noHp;
            $pendaki->tanggal_lahir = $request->tanggalLahir;
            $pendaki->user_id = $user->id;

            $pendaki->save();

            $pemesanan = Pemesanan::find($request->pemesananId);

            $pendaki->pemesanan()->attach($pemesanan,["status_pendaki" => "ketua"]);

            DB::commit();


        }catch (\Exception $exception){
            DB::rollBack();
            echo $exception->getMessage();
        }

        $response = new UserRegistrasiPendakiResponse();
        $response->pemesanan = $pemesanan;
        $response->user = $user;
        return $response;

    }

    public function login(UserLoginRequest $request): UserLoginResponse
    {
        $user = User::where("username", $request->username)->first();


        if (!$user){
            throw new UserException("username atau password salah");
        }

        if (Hash::check($request->password, $user->password)){
            $response = new UserLoginResponse();
            $response->user = $user;
            return $response;
        }else{
            throw new UserException("username atau password salah");
        }

    }

    public function addUserAdmin(AddUserAdminRequest $request): User
    {
        try {
            DB::beginTransaction();
            $user = new User();
            $user->id = uniqid("USER");
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->role = "admin";
            $user->save();

            $admin = new Admin();
            $admin->id = uniqid("ADMIN");
            $admin->nama = $request->nama;
            $admin->email = $request->email;
            $admin->nomer_hp = $request->nomer_hp;
            $admin->user_id = $user->id;
            $admin->save();

            DB::commit();
        }catch (Exception $exception){
            DB::rollBack();
            echo $exception->getMessage();
        }

        return $user;

    }


    public function accountRegister(AccountRegisterRequest $request): User
    {
        // cek apakah sudah terdaftar pendaki
        $pendaki = Pendaki::where("nik", $request->nik)->first();

        if($pendaki == null){
            throw new UserException("Anda Belum terdaftar sebagai pendaki, silahkan daftar sebagai ketua pendaki atau konfirmasi ketua");
        }

        try {
            DB::beginTransaction();

            $user = new User();
            $user->id = uniqid("USER");
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->role = "user";
            $user->save();

            $pendaki->user_id = $user->id;
            $pendaki->save();

            DB::commit();
        }catch (Exception $exception){
            DB::rollBack();
            echo $exception->getMessage();
        }

        return $user;
    }
}
