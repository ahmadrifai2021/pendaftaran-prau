<?php

namespace App\Service\Impl;

use App\Exceptions\PendakiException;
use App\Http\Requests\PendakiTambahAnggotaByNikRequest;
use App\Http\Requests\PendakiTambahAnggotaRequest;
use App\Model\Pemesanan;
use App\Model\Pendaki;
use App\Model\Response\PendakiTambahAnggotaResponse;
use App\Service\PendakiService;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;

class PendakiServiceImpl implements PendakiService
{

    public function tambahAnggota(PendakiTambahAnggotaRequest $request): PendakiTambahAnggotaResponse
    {

        try {
            DB::beginTransaction();
            $pendaki = new Pendaki();
            $pendaki->id = uniqid("PD");
            $pendaki->nik = $request->nik;
            $pendaki->nama = $request->nama;
            $pendaki->alamat = $request->alamat;
            $pendaki->jenis_kelamin = $request->jenisKelamin;
            $pendaki->nomer_hp = $request->noHp;
            $pendaki->tanggal_lahir = $request->tanggalLahir;
            $pendaki->save();

            $pemesanan = Pemesanan::find($request->pemesananId);

            $pendaki->pemesanan()->attach($pemesanan,["status_pendaki" => "angggota"]);

            DB::commit();

        }catch (Exception $exception){
            DB::rollBack();
            echo $exception->getMessage();
        }

        $response = new PendakiTambahAnggotaResponse();
        $response->pendaki = $pendaki;
        $response->pemesanan = $pemesanan;
        return $response;
    }

    public function tambahAnggotaByNIK(PendakiTambahAnggotaByNikRequest $request): PendakiTambahAnggotaResponse
    {
        $pemesanan = Pemesanan::find($request->pemesananId);
        $pendaki = Pendaki::where("nik" , $request->nik)->first();

//        dd($pendaki);
        $ps = $pemesanan->pendaki()->where("nik", $request->nik)->first();



        if ($ps != null){
            throw new PendakiException("NIK sudah terdaftar dalam pemesanan tiket");
        }

        $pemesanan->pendaki()->attach($pendaki,["status_pendaki" => "anggota"]);

        $respone = new PendakiTambahAnggotaResponse();
        $respone->pendaki = $pendaki;
        $respone->pemesanan = $pemesanan;

        return $respone;
    }
}
