<?php

namespace App\Service\Impl;

use App\DTO\Response\PemesananCreateResponse;
use App\Exceptions\PemesananNotFoundException;
use App\Http\Requests\BayarPemesananRequest;
use App\Http\Requests\PemesananCreateRequest;
use App\Http\Requests\PemesananTiketRequest;
use App\Model\Pemesanan;
use App\Model\PemesananModel;
use App\Model\Response\BayarPemesananResponse;
use App\Model\Response\PemesananTiketResponse;
use App\Model\Tiket;
use App\Model\TiketModel;
use App\Service\PemesananService;
use App\Util\GenerateQrCodeUtil;

class PemesananServiceImpl implements PemesananService
{

    public function tambah(PemesananTiketRequest $request): PemesananTiketResponse
    {
        $tiket = Tiket::find($request->tiketId);



        $pemesanan = new Pemesanan();
        $pemesanan->id = uniqid("PS");
        $pemesanan->tanggal_berangkat = $request->tanggalBerangkat;
        $pemesanan->tanggal_pulang = $request->tanggalPulang;
        $qrCode = GenerateQrCodeUtil::generate(route("pemesanan.bayar", ["id" => $pemesanan->id]),uniqid("QR-"));
        $pemesanan->qr_code = $qrCode;

        $tiket->pemesanan()->save($pemesanan);

        $pemesananResponse = Pemesanan::find($pemesanan->id);

        $response = new PemesananTiketResponse();
        $response->pemesanan = $pemesananResponse;
        return $response;
    }

    public function bayar(BayarPemesananRequest $request): BayarPemesananResponse
    {
        //cek pemesanan sudah terbayar?
        $pemesanan = Pemesanan::find($request->id_pemesanan);

        if ($pemesanan == null){
            throw new PemesananNotFoundException("Pemesanan tidak ditemukan");
        }

        $pemesanan->status_pembayaran = "TERBAYAR";
        $pemesanan->save();

        $response = new BayarPemesananResponse();
        $response->pemesanan = $pemesanan;

        return $response;

    }
}
