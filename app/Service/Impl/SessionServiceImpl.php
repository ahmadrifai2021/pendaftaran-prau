<?php

namespace App\Service\Impl;

use App\Model\Admin;
use App\Model\Pendaki;
use App\Model\Session;
use App\Model\User;
use App\Service\SessionService;

class SessionServiceImpl implements SessionService
{

    public static string $COOKIE_NAME = "X-PENDAFTARAN-PENDAKIAN";

    public function create(string $userId): Session
    {
        $session = new Session();
        $session->id = uniqid("SESSION");
        $session->user_id = $userId;
        $session->save();

        setcookie(self::$COOKIE_NAME, $session->id, time() + (60 * 60 * 24 * 30), "/");

        return $session;
    }

    public function current(): ?Pendaki
    {
        $sessionId = $_COOKIE[self::$COOKIE_NAME] ?? "";

        $session = Session::find($sessionId);



        if (!$session){
            return null;
        }

        $pendaki = Pendaki::where("user_id", $session->user_id)->first();

        return $pendaki;

    }

    public function destroy(): void
    {
        $sessionId = $_COOKIE[self::$COOKIE_NAME] ?? "";
        $session = Session::find($sessionId);
        $session->delete();
        setcookie(self::$COOKIE_NAME, '', 1, "/");
    }

    public function currentAdmin(): ?Admin
    {
        $sessionId = $_COOKIE[self::$COOKIE_NAME] ?? "";

        $session = Session::find($sessionId);



        if (!$session){
            return null;
        }

        $admin = Admin::where("user_id", $session->user_id)->first();

        return $admin;
    }
}
