<?php

namespace App\Service;

use App\Http\Requests\AccountRegisterRequest;
use App\Http\Requests\AddUserAdminRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrasiPendakiRequest;
use App\Model\Response\UserFormRegistrasiPendakiResponse;
use App\Model\Response\UserLoginResponse;
use App\Model\Response\UserRegistrasiPendakiResponse;
use App\Model\User;

interface UserService
{
    public function formRegistrasiPendaki(string $pemesananId) : UserFormRegistrasiPendakiResponse;
    public function registrasiPendaki(UserRegistrasiPendakiRequest $request) : UserRegistrasiPendakiResponse;
    public function login(UserLoginRequest $request) : UserLoginResponse;
    public function addUserAdmin(AddUserAdminRequest $request): User;
    public function accountRegister(AccountRegisterRequest $request): User;
}
