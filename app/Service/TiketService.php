<?php

namespace App\Service;

use App\Http\Requests\AddTiketRequest;
use App\Model\Tiket;

interface TiketService
{
    public function add(AddTiketRequest $request) : Tiket;
    public function delete(string $id): Tiket;
}
