<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pendaki extends Model
{
    //
    protected $table = "pendaki";
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["id", "nik", "nama", "alamat", "tanggal_lahir", "jenis_kelamin", "user_id", "nomer_hp"];

    public function pemesanan()
    {
        return $this->belongsToMany(Pemesanan::class)->withPivot("status_pendaki");
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

}
