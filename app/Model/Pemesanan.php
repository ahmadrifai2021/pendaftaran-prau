<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    //
    protected $table = "pemesanan";
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["id", "tanggal_berangkat", "tanggal_pulang", "tiket_id"];

    public function tiket()
    {
        return $this->belongsTo(Tiket::class);
    }

    public function pendaki()
    {
        return $this->belongsToMany(Pendaki::class)->withPivot("id","status_pendaki");
    }

}
