<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    //
    protected $table = "tiket";
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["id", "label", "harga"];

    public function pemesanan(){
        return $this->hasMany(Pemesanan::class);
    }
}
