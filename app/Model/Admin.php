<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = "admin";
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["id", "nama", "email", "nomer_hp"];

    public function user()
    {
        return $this->hasOne(User::class);
    }

}
