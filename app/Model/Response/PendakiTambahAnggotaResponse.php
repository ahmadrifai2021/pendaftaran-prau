<?php

namespace App\Model\Response;

use App\Model\Pemesanan;
use App\Model\Pendaki;

class PendakiTambahAnggotaResponse
{
    public Pendaki $pendaki;
    public Pemesanan $pemesanan;
}
