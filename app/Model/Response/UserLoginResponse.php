<?php

namespace App\Model\Response;

use App\Model\User;

class UserLoginResponse
{
    public User $user;
}
