<?php

namespace App\Model\Response;

use App\Model\Pemesanan;
use App\Model\User;

class UserRegistrasiPendakiResponse
{
    public Pemesanan $pemesanan;
    public User $user;
}
