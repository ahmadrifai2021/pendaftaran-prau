<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["id", "username", "password", "role"];

    public function pendaki(){
        return $this->hasOne(Pendaki::class);
    }

    public function admin(){
        return $this->hasOne(Pendaki::class);
    }

}
