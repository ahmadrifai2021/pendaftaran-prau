<?php

namespace App\Http\Controllers;

use App\Model\Pemesanan;
use App\Model\Pendaki;
use App\Model\Tiket;
use App\Model\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $totalPemesanan = Pemesanan::all()->count();
        $totalTiket = Tiket::all()->count();
        $totalUser = User::all()->count();
        $totalPemesananBaru = Pemesanan::where("status_pembayaran", "BELUM_BAYAR")->get()->count();
        return view("admin.dashboard",[
            "title" => "Dashboard",
            "totalPemesanan" => $totalPemesanan,
            "totalTiket" => $totalTiket,
            "totalUser" => $totalUser,
            "totalPemesananBaru" => $totalPemesananBaru
        ]);
    }

    public function pemesanan()
    {
        $pemesanan = Pemesanan::paginate(10);
        return view("admin.pemesanan", [
            "title" => "Daftar Pemesanan",
            "pemesanan" => $pemesanan
        ]);
    }

    public function tiket()
    {
        $tiket = Tiket::paginate(10);
        return view("admin.tiket",[
            "title" => "Daftar Tiket",
            "tiket" => $tiket
        ]);
    }

    public function pendaki()
    {
        $pendaki = Pendaki::paginate(10);
        return view("admin.pendaki", [
            "title" => "Daftar Pendaki",
            "pendaki" => $pendaki
        ]);
    }

    public function user()
    {
        $user = User::where("role", "user")->paginate(10);
        $admin = User::where("role", "admin")->get();
        return view("admin.user", [
            "title" => "Daftar User",
            "admin" => $admin,
            "user" => $user
        ]);
    }


}
