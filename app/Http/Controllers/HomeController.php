<?php

namespace App\Http\Controllers;

use App\Exceptions\PemesananNotFoundException;
use App\Model\Pemesanan;
use App\Model\Pendaki;
use App\Service\Impl\SessionServiceImpl;
use App\Service\SessionService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    private SessionService $sessionService;

    public function __construct()
    {
        $this->sessionService = new SessionServiceImpl();
    }

    public function index()
    {
        return view("home.index");
    }

    public function sop()
    {
        return view("home.sop");
    }

    public function postSop(Request $request){
        $request->validate([
            "setuju" => "required"
        ]);

        return redirect()->route("pemesanan.tambah");
    }

    public function dashboard()
    {

        $pendaki = $this->sessionService->current();
        $pemesanan = $pendaki->pemesanan()->where("status_pendaki", "ketua")
            ->where("status_pembayaran", "BELUM_BAYAR")->get();


        if($pemesanan->first() == null){

            return view("home.dashboard", [
                "title" => "Dashboard",
                "pendaki" => $pendaki,
                "pemesanan" => $pemesanan,
                "pemesananNotFound" => "Belum ada pemesanan"
            ]);
        }else{
            return view("home.dashboard", [
                "title" => "Dashboard",
                "pendaki" => $pendaki,
                "pemesanan" => $pemesanan,
            ]);
        }


    }


}
