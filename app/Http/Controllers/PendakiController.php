<?php

namespace App\Http\Controllers;

use App\Exceptions\PendakiException;
use App\Exceptions\UserException;
use App\Http\Requests\PendakiTambahAnggotaByNikRequest;
use App\Http\Requests\PendakiTambahAnggotaRequest;
use App\Model\Pemesanan;
use App\Model\Pendaki;
use App\Service\Impl\PendakiServiceImpl;
use App\Service\Impl\SessionServiceImpl;
use App\Service\PendakiService;
use App\Service\SessionService;

class PendakiController
{
    private PendakiService $pendakiService;
    private SessionService $sessionService;

    public function __construct()
    {
        $this->pendakiService = new PendakiServiceImpl();
        $this->sessionService = new SessionServiceImpl();
    }


    public function cekNik(PendakiTambahAnggotaByNikRequest $request)
    {
        $pendaki = Pendaki::where("nik", $request->nik)->first();

        try {
            if (isset($pendaki->id)){
                $this->pendakiService->tambahAnggotaByNIK($request);
                return redirect()->back();
            }else{
                return redirect()->route("pendaki.tambah.anggota", [
                    "pemesananId" => $request->pemesananId
                ])->with(["nik" => $request->nik]);
            }
        }catch (PendakiException $exception){
            $pendaki = $this->sessionService->current();
            $pemesanan = $pendaki->pemesanan()->where("status_pendaki", "ketua")->get();
            return back()->with(["error" => $exception->getMessage()]);
        }

    }

    public function tambahAnggota($pemesananId)
    {
//        $pendaki = $this->sessionService->current();
        return view("pendaki.tambah-anggota", [
            "pemesananId" => $pemesananId
        ]);
    }

    public function postTambahAnggota(PendakiTambahAnggotaRequest $request, $pemesananId)
    {
        try {
            $this->pendakiService->tambahAnggota($request);
            return redirect()->route("home.dashboard");
        }catch (UserException $exception){
            return back()->with(["error" => $exception->getMessage()]);
        }
    }



}
