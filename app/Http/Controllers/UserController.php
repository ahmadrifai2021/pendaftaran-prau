<?php

namespace App\Http\Controllers;

use App\Exceptions\PemesananNotFoundException;
use App\Exceptions\UserException;
use App\Http\Requests\AccountRegisterRequest;
use App\Http\Requests\AddUserAdminRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrasiPendakiRequest;
use App\Model\Pemesanan;
use App\Model\Session;
use App\Service\Impl\SessionServiceImpl;
use App\Service\Impl\UserServiceImpl;
use App\Service\SessionService;
use App\Service\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserService $userService;
    private SessionService $sessionService;

    //
    public function __construct()
    {
        $this->userService = new UserServiceImpl();
        $this->sessionService = new SessionServiceImpl();
    }

    public function login()
    {
        return view("user/login", [
            "title" => "User Login"
        ]);
    }

    public function postLogin(UserLoginRequest $request)
    {

        try {
            $response = $this->userService->login($request);

            if ($response->user->role == "user"){
                $this->sessionService->create($response->user->id);
                return redirect()->route("home.dashboard");
            }else{
                $this->sessionService->create($response->user->id);
                return redirect()->route("admin.dashboard");
            }
        }catch (UserException $exception){
            return back()->with(["error" => $exception->getMessage()]);
        }

    }

    public function formRegistrasi($pemesanan){
        try {
            $response = $this->userService->formRegistrasiPendaki($pemesanan);
            return view("user/registrasi", [
               "pemesananId" => $response->pemesanan->id
            ]);
        }catch (PemesananNotFoundException $exception){
            return $exception->getMessage();
        }
    }

    public function postRegistrasi(UserRegistrasiPendakiRequest $request)
    {
        $response = $this->userService->registrasiPendaki($request);
        $this->sessionService->create($response->user->id);
        return redirect()->route("home.dashboard");
    }

    public function logout()
    {
        $this->sessionService->destroy();
        return redirect()->route("user.login");
    }

    public function addAdmin(AddUserAdminRequest $request)
    {
        $response = $this->userService->addUserAdmin($request);
        return back()->with("status", "Berhasil Menambah data Admin");
    }

    public function registerAccount()
    {
        return view("user.register-account", [
            "title" => "Registrasi User"
        ]);
    }
    public function postRegisterAccount(AccountRegisterRequest $request)
    {
        try {
            $response = $this->userService->accountRegister($request);
            return redirect()->route("user.login");
        }catch (UserException $exception){
            return back()->with(["error" => $exception->getMessage()]);
        }
    }
}
