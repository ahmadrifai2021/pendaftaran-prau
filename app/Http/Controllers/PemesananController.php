<?php

namespace App\Http\Controllers;

use App\Exceptions\PemesananNotFoundException;
use App\Http\Requests\BayarPemesananRequest;
use App\Http\Requests\PemesananTiketRequest;
use App\Model\Pemesanan;
use App\Model\Tiket;
use App\Service\Impl\PemesananServiceImpl;
use App\Service\Impl\SessionServiceImpl;
use App\Service\PemesananService;
use App\Service\SessionService;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class PemesananController extends Controller
{
    private PemesananService $pemesananService;
    private SessionService $sessionService;

    //
    public function __construct()
    {
        $this->pemesananService = new PemesananServiceImpl();
        $this->sessionService = new SessionServiceImpl();
    }

    public function tambah()
    {
        $tikets = Tiket::all();
        return view("pemesanan.tiket", [
            "title" => "Pemesanan Tiket",
            "tikets" => $tikets
        ]);
    }

    public function postTambah(PemesananTiketRequest $request)
    {
        $response = $this->pemesananService->tambah($request);
        return redirect("/user/registrasi/" . $response->pemesanan->id);
    }

    public function cetakPdf($pemesananId)
    {
        $pendaki = $this->sessionService->current();
        $pemesanan = $pendaki->pemesanan()->where("status_pendaki", "ketua")->get();

        return view("pemesanan.cetak-pdf", [
            "pemesanan" => $pemesanan
        ]);
    }

    public function hapus(string $id){
        DB::table("pemesanan_pendaki")->delete($id);

        return back();
    }

    public function riwayat()
    {
        $pendaki = $this->sessionService->current();

        $pemesanan = $pendaki->pemesanan()->get();

        return view("pemesanan.riwayat", [
            "title" => "Riwayat Pemesanan",
            "pemesanan" => $pemesanan
        ]);
    }

    public function bayar(string $id)
    {

        $pemesanan = Pemesanan::find($id);

        return view("admin.bayar", [
            "title" => "Bayar Pemesanan",
            "pemesanan" => $pemesanan
        ]);
    }

    public function postBayar(BayarPemesananRequest $request){
        try {
            $this->pemesananService->bayar($request);
            return redirect()->route("pemesanan.bayar", ["id" => $request->id_pemesanan]);
        }catch (PemesananNotFoundException $exception){
            return redirect()->route("pemesanan.bayar")->with("error", $exception->getMessage());
        }
    }


}
