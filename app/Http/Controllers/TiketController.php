<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddTiketRequest;
use App\Service\Impl\TiketServiceImpl;
use App\Service\TiketService;
use Illuminate\Http\Request;

class TiketController extends Controller
{
    private TiketService $tiketService;

    public function __construct()
    {
        $this->tiketService = new TiketServiceImpl();
    }

    public function add(AddTiketRequest $request)
    {
        $this->tiketService->add($request);
        return back()->with("status", "Berhasil menambah tiket");
    }

    public function delete(string $id)
    {
        $this->tiketService->delete($id);
        return back()->with("status", "Berhasil menghapus tiket");
    }

}
