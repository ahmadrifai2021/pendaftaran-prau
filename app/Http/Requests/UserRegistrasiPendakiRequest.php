<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegistrasiPendakiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "pemesananId" => "requiered",
            "nama" => "required",
            "nik" => "required|max:16|unique:pendaki",
            "alamat" => "required",
            "noHp" => "required",
            "jenisKelamin" => "required",
            "tanggalLahir" => "required",
            "username" => "required|unique:user",
            "password" => "required"
        ];
    }
}
