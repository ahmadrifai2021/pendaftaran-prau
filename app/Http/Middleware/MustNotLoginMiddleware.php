<?php

namespace App\Http\Middleware;

use App\Service\Impl\SessionServiceImpl;
use App\Service\SessionService;
use Closure;

class MustNotLoginMiddleware
{

    private SessionService $sessionService;

    public function __construct()
    {
        $this->sessionService = new SessionServiceImpl();
    }


    public function handle($request, Closure $next)
    {
        $pendaki = $this->sessionService->current();

        if ($pendaki){
            return redirect()->route("home.dashboard");
        }

        return $next($request);

    }
}
