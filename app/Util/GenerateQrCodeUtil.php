<?php

namespace App\Util;


use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GenerateQrCodeUtil
{

    public static function generate(string $content, string $name):string
    {

        $qr = QrCode::format("svg")->generate($content);

        $path = "/qrcode/" . $name .".svg";

        Storage::disk("public")->put("qrcode/". $name .".svg", $qr);

        return $path;
    }
}
